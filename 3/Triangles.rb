for i in (1..6)
  i = "#{ "*" * i}"
  puts i
end

for i in (1..6)
  i = "#{ "*" * i}".rjust(20)
  puts i
end


multiplikator = 11
for i in (1..6).to_a
  puts "#{ "*" * multiplikator}".center(20)
  multiplikator = multiplikator - 2
end