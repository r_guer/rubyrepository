puts "Please enter number"
number = gets.chomp.to_i

amount = 0
while number >= 2 do
  number = number / 2
  amount = amount + 1  
end
puts "#{amount} is the amount of times your number can be divided by 2"

