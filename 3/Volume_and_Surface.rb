puts "Please enter radius"
radius = gets.chomp.to_f

case radius
  when 0..radius
    Volume = 4 / 3 * Math::PI * (radius**3)
    puts "Volume = #{Volume}"
    Surface = 4 * Math::PI * (radius**2)
    puts "Surface = #{Surface}"
  end