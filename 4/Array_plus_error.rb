puts "Please enter number"
number = gets.chomp.to_i

array = ['zero', 1, 2, 'three', 'four', 5, 6, 7, 'eight']

begin
  result = array.sample + number
 rescue TypeError
  puts "Integer cannot be added to string"
 else
  puts result
 end
