puts "Enter DNA sequence"
sequence = gets.chomp.upcase

if sequence.include? 'U'
  raise ArgumentError.new "You entered RNA instead of DNA!"
end

puts sequence.length.to_i
length = sequence.length.to_i

puts "#{sequence.count("A") / sequence.length.to_f}" "for A"
puts "#{sequence.count("T") / sequence.length.to_f}" "for T"
puts "#{sequence.count("G") / sequence.length.to_f}" "for G"
puts "#{sequence.count("C") / sequence.length.to_f}" "for C"

if
  length < 14
  short = 4 * sequence.count("G") + sequence.count("C") + 2 * sequence.count("A") + sequence.count("T")
  puts "The melting temperature is #{short}"
else
  long = 64.9 + 41 * sequence.count("G") + sequence.count("C") - 16.4 / sequence.length.to_i
  puts "The melting temperature is #{long}"
end 



