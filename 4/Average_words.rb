puts "Please enter sentence"
sentence = gets.chomp
words = sentence.split()
letters = 0 

words.each do |word|
  letters += word.length
end

puts "Average word length is #{(letters.to_f/words.length).round(2)}"