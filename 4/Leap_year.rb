puts "Please enter year"
year = gets.chomp.to_i 

if (year % 4 == 0)
  if (year % 100 != 0)
    puts "#{year} is a leap year"
  else
    if (year % 400 == 0)
      puts "#{year} is a leap year"
    else
      puts "#{year} is not a leap year"
    end
  end
else
  puts "#{year} is not a leap year"  
end