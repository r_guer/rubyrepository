sequences = {}
name = ""
comment = ""
sequence = ""

File.open("Angiosperms.fasta", "r").each do |line|
  line = line.chomp
  if line.start_with?(">")
    if name != ""
      sequences[name] = sequence
      sequence = ""
    end
    name = line.delete(">")  
  elsif line.start_with?(";")
    comment = line.delete(";")
    #puts comment
  else
    sequence << line#.delete("-") 
  end
end
sequences[name] = sequence
#puts sequences["Ginkgo"].size
#puts sequences.size

sum = 0
sequences.values.each do |sequence|
  length = sequence.delete("-").length
  sum += length
end
puts (sum.to_f / sequences.length).round(3)


#With GC content
sequences = {}
name = ""
comment = ""
sequence = ""

File.open("Angiosperms.fasta", "r").each do |line|
  line = line.chomp
  if line.start_with?(">")
    if name != ""
      sequences[name] = sequence
      sequence = ""
    end
    name = line.delete(">")  
  elsif line.start_with?(";")
    comment = line.delete(";")
    #puts comment
  else
    sequence << line#.delete("-") 
  end
end
sequences[name] = sequence
#puts sequences["Ginkgo"].size
#puts sequences.size

gc_content = 0
sequences.values.each do |sequence|
  length = sequence.delete("-").length
  gc_content += (sequence.count("G") + sequence.count("C"))/length.to_f
end
puts (gc_content / sequences.length).round(5)


p sequences


File.open("Pfannkuchen.fasta", "w") do |file|
  sequences.each do |name, sequence|
    name = ">" + name
    file.puts name
    file.puts sequence.scan(/.{0,80}/)
    #file.puts sequence
  end
end


