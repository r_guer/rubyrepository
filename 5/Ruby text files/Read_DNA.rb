File.open("DNA_sequence.txt", "r").each do |line|
  sequence = line
  complement = ""

  bases = {'A' => 'T', 
          'T' => 'A', 
          'C' => 'G', 
          'G' => 'C' }

    (0...sequence.length).each do |index|
      current_base = sequence[index]
      complement <<  bases[current_base]
    end
    puts "Reverse complement: #{complement.reverse}"
end
